import api from "../api/api"
export const fetchCategory = async () => {
    let response = await api.get('author')
    return response.data.data

}

//Fetch All Author
export const fetchAllAuthors = async () => {
    try {
      const results = await api.get("author");
      return results.data.data;
    } catch (err) {
      console.log("Error: ",err); 
    }
  };
  
  
  //Delete Author by Id
  export const deleteAuthorById = async (id) => {
    try {
      const result = await api.delete(`author/${id}`);
      console.log("FetchAllArticle: ", result.data.data);
      return result.data.message;
    } catch (error) {
      console.log("FetchAllArticle Error:", error);
    }
  };
  
  //Upload Image
  export const uploadImage = async (file) => {
    let formData = new FormData();
    formData.append("image", file);
  
    let response = await api.post("images", formData);
    return response.data.url;
  };
  
  //Put Author
  export const putAuthor = async (_id, author) => {
    try {
      const result = await api.put(`author/${_id}`, author);
      console.log("FetchAllArticle:", result.data.message);
      return result.data.message;
    } catch (error) {
      console.log("FetchAllArticle Error:", error);
    }
  };

  // Post Author
  export const postAuthor = async (author) => {
    try {
      const result = await api.post("author", author);
      console.log("FetchAllArticle:", result.data.message);
      return result.data.message;
    } catch (error) {
      console.log("FetchAllArticle Error:", error);
    }
  };
  
  